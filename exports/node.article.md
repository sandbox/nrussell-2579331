##Content Type ##

| Field | Type | Description | Required |
| -- | -- | -- | -- |
| Body | text_textarea_with_summary |  |  |
| Image | image_image | Upload an image to go with this article. |  |
| Tags | taxonomy_autocomplete | Enter a comma-separated list of words to describe your content. |  |
