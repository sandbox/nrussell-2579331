-- SUMMARY --
The Bundle Markdown module provides a drush utility command which allows you
to export various entity types and their bundles into markdown format for use
in Gitbooks or other markdown supported reporting tools. It was designed to
provide a means of generating architectural documentation of ones Drupal site.

-- REQUIREMENTS --
Drush v7+ installed.

-- INSTALLATION --
* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --
None.

-- CUSTOMIZATION --
None.

-- TROUBLESHOOTING --
* If the drush command 'bme' is unavailable:
  - Ensure you have drush installed locally.
  - Run: drush cc drush

-- FAQ --
None.

-- CONTACT --
* Nick Russell (therealwebguy) - https://www.drupal.org/user/123931
