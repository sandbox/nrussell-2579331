<?php

/**
 * Implements of hook_drush_help().
 */
function bundle_markdown_drush_help($section) {
  switch ($section) {
    case 'drush:bundle-markdown-export':
      return dt('Export a content type to markdown. Usage: drush bundle_markdown-ct-export article');
  }
}

/**
 * Implements of hook_drush_command().
 */
function bundle_markdown_drush_command() {
  $items = array();

  $items['bundle-markdown-export'] = array(
    'description' => 'Export a bundle to markdown.',
    'aliases' => array('bme'),
    'arguments' => array(
      'entity_type' => 'The type of enty to export.',
      'bundle' => 'The bundle to export. If omitted, all bundles will be exported,',
    ),
  );

  return $items;
}

/**
 * Export bundle command.
 */
function drush_bundle_markdown_export($entity_type = 'node', $bundle = NULL) {

  // TODO: We need to do a switch on the entity_type once we allow users to pass
  // options other than node before we start getting node types.
  $node_types = node_type_get_types();
  $bundles = array();

  if (!empty($bundle)) {
    if (!$node_types[$bundle]) {
      drush_set_error('bundle_markdown NODE TYPES:', dt("No bundle of type {$bundle} was found."));
    }
    $node_type = node_type_get_type($bundle);
    $node_types = array(
      $bundle => $node_type,
    );
  }

  // Iterate through all of the node types.
  if ($node_types) {
    foreach ($node_types as $node_type => $bundle) {
      if (!empty($node_type) && is_string($node_type)) {
        $instances = field_info_instances($entity_type, $node_type);
        ksort($instances);
        $bundles[$node_type] = new stdClass();
        $bundles[$node_type]->instances = $instances;
      }
    }
  }

  // Let the user know which bundles are going to be exported and let
  // them confirm that is what they want to do.
  $i = 1;
  $total_bundles = count($bundles);
  $confirmation_output = "The following node types will be exported: ";
  foreach ($bundles as $node_type => $bundle) {
    $tag = ($i < $total_bundles) ? "," : "";
    $confirmation_output.= "{$node_type}{$tag} ";
    $i++;
  }

  if (!drush_choice(array(1 => 'Continue'), $confirmation_output)) {
    exit;
  }

  // Prepare to write file.
  $dir = drupal_get_path('module', 'bundle_markdown') . '/exports';
  if (!file_prepare_directory($dir, FILE_CREATE_DIRECTORY) && !mkdir($dir, 0775, TRUE)) {
    drush_set_error('bundle_markdown_COPY_FILE_SAVE', dt('Could not create exports directory.'));
    return;
  }


  $output = bundle_markdown_convert_to_markdown($bundles);

  if ($output) {
    foreach ($output as $type => $markdown) {
      $file_path = $dir . "/{$entity_type}.{$type}.md";
      $handle = fopen($file_path, "w");
      if ($handle === FALSE) {
        drush_set_error('bundle_markdown_COPY_FILE_SAVE', dt('Could not open export file for writing.'));
        return;
      }

      $result = drush_op('fwrite', $handle, $markdown);
      print $result;
    }
  }
}
